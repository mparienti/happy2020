<html>
    <head>
        <title>Happy 2020</title>
        <meta charset="utf-8" />
        <style>
         #main {
           margin: 0 auto;
           display: grid;
           grid-template-columns: 30% 70%;
           grid-template-rows: 70% 30%;
           width: 1022px;
           height: 533px;
           background-image: url("./img/bgh1.jpeg");
         }
         #message {
           grid-column-start: 2;
           grid-row-start: 1;
           place-items: stretch stretch;
           display: grid;
         }
         #focus {
           position: relative;
           width: 250px;
           height: 250px;
           top: 250px;
           left: 25px;
           background-repeat: no-repeat;
           background-position: top center;
           /* transition: background 0.6s ease-in-out; */
         }
         .focus_on {
           background: url('img/bg_big_sprite.jpeg') no-repeat;
         }
         .case {
           /* you can addd you own stuff here*/
         }
         .case_on {
           background: url('img/bg_sprite.png') no-repeat;
         }
        </style>
        <script>
         const message_string = `
*  *  **  ***  ***  * * 
*  * *  * *  * *  * * *
**** **** ***  ***   *
*  * *  * *    *     *
*  * *  * *    *     *

  **    **   **   **
 *  *  *  * *  * *  *
   *   *  *   *  *  *
  *    *  *  *   *  *
 ****   **  ****  **
         `;
         const size = {total_images};
         const img_height = 40;
         const bigimg_height = 250;
         const images = [...Array(size).keys()];

         buildArray = (st) => {
           let max_width = 0,
               count = 0,
               i = 0,
               line = [];
           const result = [];
           for (char of st) {
             if (char === undefined) {

               continue;
             }
             if (char == "\n") {
               result.push(line);
               i = 0;
               line = [];
               continue;
             }
             i++;
             line.push(char);
             if (char != ' ') {
               count++;
             }
             if (i > max_width) {
               max_width = i;
             }
           }
           result.push(line);
           return [max_width, count, result];
         };
         createGrid = (div, cols, rows) => {
           for (let r = 0; r < rows; r++) {
             for (let c = 0; c < cols; c++) {
               let el = document.createElement("div");
               el.setAttribute('id', `c_${c}_${r}`);
               el.setAttribute('class', 'case');
               el.style.gridColumnStart = c+1;
               el.style.gridRowStart = r+1;
               div.appendChild(el);
             }
           }
         };
         fillGrid = (msg, focus, imgs) => {
           let count = 0;
           for (let r = 0; r < msg.length; r++) {
             let line = msg[r];
             for (let c = 0; c < line.length; c++) {
               if (line[c] != ' ' && line[c] != "\n" ) {
                 let el = document.getElementById(`c_${c}_${r}`);
                 let sprite_position = imgs[count] * img_height;
                 let bigsprite_position = imgs[count] * bigimg_height;
                 el.classList.add("case_on");
                 el.style.backgroundPosition = `0px -${sprite_position}px`;
                 el.addEventListener("mouseover", function( event ) {
                   focus.classList.add("focus_on");
                   focus.style.backgroundPosition = `0px -${bigsprite_position}px`;
                 }, false);
                 el.addEventListener("mouseout", function( event ) {
                   focus.style.backgroundImage = '';
                   focus.classList.remove("focus_on");
                 }, false);
                 el.addEventListener("click", function( event ) {
                   focus.classList.add("focus_on");
                   focus.style.backgroundPosition = `0px -${bigsprite_position}px`;
                 }, false);

                 count++;
               }
             }
           }
         };
         // Credit: https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
         shuffleArray = (array) => {
           for (let i = array.length - 1; i > 0; i--) {
             const j = Math.floor(Math.random() * (i + 1));
             [array[i], array[j]] = [array[j], array[i]];
           }
         };
         function on_load() {
           const [max_width, count, message] = buildArray(message_string);
           if (count > images.length ) {
             throw 'Not enough images';
           }
           const div = document.getElementById('message');
           const focus = document.getElementById('focus');
           const main = document.getElementById('main');
           createGrid(div, max_width, message.length);
           shuffleArray(images);
           fillGrid(message, focus, images);

           main.addEventListener("click", function( event ) {
             focus.style.backgroundImage = '';
             focus.classList.remove("focus_on");
           }, true);
           window.setInterval(() => {shuffleArray(images);fillGrid(message, focus, images);}, 45000);
         }
         document.addEventListener("DOMContentLoaded", on_load);
         const img = new Image();
         img.src = 'img/bg_big_sprite.jpeg';
        </script>
  </head>
  <body>
    <div id="main">
      <div id="message">
      </div>
      <div id="focus">
      </div>
    </div>
  </body>
</html>
