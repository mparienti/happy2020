#!/bin/bash
set -o pipefail
set -e

CURRENT=$(dirname "$(readlink -f "$0")")
TARGET=`pwd`/public
BUILD=`pwd`/build

mkdir  ${BUILD} ${TARGET}/img -p

cd ${CURRENT}/src/img/photos


# Resize big images
i=0
for file in *.* ;
do
    i=$((${i}+1))
    convert "${file}" -resize 40x40^ -gravity Center -extent 40x40 ${BUILD}/img_${i}.jpeg
    convert "${file}" -resize 250x250^ -gravity Center -extent 250x250 ${BUILD}/img_big_${i}.jpeg
done

NB_IMAGES=$i

echo Found ${NB_IMAGES} images

# Build mosaic
cd ${BUILD}
montage -tile 1x${NB_IMAGES}  -geometry +0+0 img_[1-9]*.jpeg ${TARGET}/img/bg_sprite.png
montage -tile 1x${NB_IMAGES}  -geometry +0+0 img_big_[1-9]*.jpeg ${TARGET}/img/bg_big_sprite.jpeg

# cp background
cd ${CURRENT}
cp ${CURRENT}/src/img/background/bgh1.jpeg ${TARGET}/img/


# template
cd ${CURRENT}/src/
cat index.html.tpl | sed 's/{total_images}/'${NB_IMAGES}'/' > ${TARGET}/index.html
